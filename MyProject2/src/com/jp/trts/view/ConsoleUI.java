package com.jp.trts.view;

import com.jp.trts.TestSystem;

import java.util.Scanner;

/**
 * This class adds console interface for users whichever group of users logging in
 */
public class ConsoleUI {
    /**
     * This method creates console interface which opens access for students user group
     * Students can view test name and how many test questions from test array.
     */
    public static void viewStudentUI() {
        System.out.println("Меню выбора теста. ");
        System.out.println("Выбирите нужный тест: ");
        for (int i = 0; i < TestSystem.Database.test.size(); i++) {
            int j = i + 1;
            System.out.println(j + ". " + TestSystem.Database.test.get(i).testId);
        }
        Scanner ch = new Scanner(System.in);
        byte choice = (byte) (ch.nextByte() - 1);
        int j = choice + 1;
        System.out.println(j + ". " + TestSystem.Database.test.get(choice).testId + ":" + "\n" + "    Количество вопросов: "
                + TestSystem.Database.test.get(choice).question.size());
    }


    /**
     * This method creates console interface which opens access for teachers user group.
     * Teachers can view tests and tests result from test array.
     */
    public static void viewTeacherUI() {
        System.out.println("Вы в меню преподавателя. " + "\n" + "Выберите тест для просмотра успеваемости: ");
        System.out.println();
        for (int i = 0; i < TestSystem.Database.test.size(); i++) {
            int j = i + 1;
            System.out.println(j + ". " + TestSystem.Database.test.get(i).testId);

        }
        Scanner ch = new Scanner(System.in);
        byte choice = (byte) (ch.nextByte() - 1);
        System.out.println(TestSystem.Database.test.get(choice).testId + " Успеваемость: " + TestSystem.Database.test.get(choice).testResult);

        System.out.println("Выберите тест для просмотра заданий");
        for (int i = 0; i < TestSystem.Database.test.size(); i++) {
            int j = i + 1;
            System.out.println(j + ". " + TestSystem.Database.test.get(i).testId);
        }
        choice = (byte) (ch.nextByte() - 1);

        for (int i = 0; i < TestSystem.Database.test.get(choice).question.size(); i++) {
            int j = i + 1;
            System.out.println(j + ". " + TestSystem.Database.test.get(choice).question.get(i).questionText + "\n" + "  Ответ: "
                    + TestSystem.Database.test.get(choice).question.get(i).correctAnswerId);
        }

    }

    /**
     * This method creates console interface which opens access for administrator user group.
     * Administrators can view user group arrays here.
     */
    public static void viewAdminUI() {
        System.out.println("Вы в меню администратора. " + "\n" + "Список пользователей: ");
        for (int i = 0; i < TestSystem.Database.user.size(); i++) {
            int j = i + 1;
            System.out.println(j + ". " + "Пользователь: " + TestSystem.Database.user.get(i).userName + ". Группа - " + TestSystem.Database.user.get(i).userId);
        }

    }

}
