package com.jp.trts.model.user;


/**
 * This class create users of Teacher user group.
 */
public class Teacher extends User {

    final private int passport;
    final private String courses;

    public Teacher(String userName, String userId, int passport, String courses) {
        super(userName, userId);
        this.courses = courses;
        this.passport = passport;
    }
}

