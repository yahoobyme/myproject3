package com.jp.trts.model.user;

/**
 * Create users of the test system. Users are divided into groups
 * with different user access and console interface.
 * This is class parent for all classes of user group.
 */
public class User {
    /**
     * @param userName name of user.
     * @param userId change user group for this user.
     */
    public String userId;
    public String userName;

    public User(String userName, String userId) {
        this.userName = userName;
        this.userId = userId;
    }
}


