package com.jp.trts.model.user;

/**
 * This class create users of Student user group.
 */
public class Student extends User {


    private int testResult;

    public int getGroupId() {
        return groupId;
    }

    final private int groupId;

    public Student(String userName, String userId, int groupId, int testResult) {
        super(userName, userId);
        this.groupId = groupId;
        this.testResult = testResult;

    }

    public int getTestResult() {
        return testResult;
    }

    public void setTestResult(int testResult) {
        this.testResult = testResult;
    }
}

