package com.jp.trts.model.user;


/**
 * This class create users of Administrator user group.
 */

public class Admin extends User {

    public Admin(String userName, String userId) {

        super(userName, userId);
    }
}

