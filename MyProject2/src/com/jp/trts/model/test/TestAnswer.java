package com.jp.trts.model.test;

/**
 * Class creates and fills arrays of answers for questions tests.
 */
public class TestAnswer {

    static String[] answerPhysic0 = {"Х. Лоренцем", "П. Ланжевеном", "А.Ф. Иоффе", "Х.К. Эрстедом"};

    static String[] answerPhysic1 = {"видимый свет", "инфракрасное излучение", "рентгеновское излучение",
            "ультрафиолетовое излучение"};

    static String[] answerPhysic2 = {"1 Вб", "1 Тл", "1 А", "1 В"};

    static String[] answerPhysic3 = {"увеличится", "уменьшится", "тень исчезнет", "не изменится"};

    static String[] answerPhysic4 = {"зарядовым", "массовым", "нуклонным", "химическим"};

    static String[] answerPhysic5 = {"частотой", "амплитудой", "периодом", "фазой"};

    static String[] answerPhysic6 = {"частоты света", "мощности светового потока", "работы выхода", "длины световой волны"};

    static String[] answerPhysic7 = {"электростатические", "ядерные", "магнитные", "гравитационные"};

    static String[] answerPhysic8 = {"косинуса угла поворота рамки", "сопротивления проволоки",
            "площади рамки", "индукции магнитного поля"};

    static String[] answerPhysic9 = {"электрогенератор", "электрическая батарейка", "конденсатор", "элемент солнечных батарей"};

    static String[] answerMath0 = {"2,6", "2,1", "2", "3,4"};

    static String[] answerMath1 = {"2648", "8244", "3924", "2016"};

    static String[] answerMath2 = {"7", "6", "8", "5"};

    static String[] answerMath3 = {"4/15", "7/30", "2/7", "3/8"};

    static String[] answerMath4 = {"-2/3 cos3х + С", "-3/2 sin2х + С", "2/3 cos3х + С", "3/2 sin2х + С"};

    static String[] answerMath5 = {"-2", "2sina", "-2cosa", "-2sina"};

    static String[] answerMath6 = {"1000/13", "830/11", "1300/17", "1307/17"};

    static String[] answerMath7 = {"8", "6", "5", "10"};

    static String[] answerMath8 = {"48", "36", "24", "28"};

    static String[] answerMath9 = {"5", "1", "2", "3"};

    static String[] answerBio0 = {"вода", "липиды", "белок", "гликоген"};

    static String[] answerBio1 = {"вода", "водород", "кислород", "спирт"};

    static String[] answerBio2 = {"кишечная палочка", "вирусная частица", "плазмида", "дрозофила"};

    static String[] answerBio3 = {"способны синтезировать органические вещества из неорганических",
            "могут синтезировать органические вещества из органических",
            "не нуждаются в источнике питания", "питаются живыми организмами"};

    static String[] answerBio4 = {"аминокислота", "белок", "нуклеотид", "углевод"};

    static String[] answerBio5 = {"гемофилией", "ишемией", "дальтонизмом", "тромбофлебитом"};

    static String[] answerBio6 = {"йод", "железо", "хром", "цинк"};

    static String[] answerBio7 = {"орган, обеспечивающий условия для развития плода", "зародыш", "оплодотворенная яйцеклетка",
            "амниотическая жидкость"};

    static String[] answerBio8 = {"вида", "отряда", "биоценоза", "биосферы"};

    static String[] answerBio9 = {"воду и минеральные вещества", "органические вещества", "минеральные вещества", "воду"};

}