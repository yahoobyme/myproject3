package com.jp.trts.model.test;

import java.util.List;

/**
 * This class create tests of test system.
 */
public class Test {

    /**
     * @param testId this is name of test
     * @param testResult results students testing
     * @param question copy of sub lists thematic questions
     */
    public String testId;
    public int testResult;
    public List<TestQuestion> question;


    public Test(String testId, int testResult, List<TestQuestion> question) {
        this.testId = testId;
        this.testResult = testResult;
        this.question = question;
    }

}

