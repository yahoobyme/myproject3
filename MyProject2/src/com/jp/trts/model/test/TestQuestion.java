package com.jp.trts.model.test;

import com.jp.trts.TestSystem;

/**
 * There are creates and fill main array of questions with answers and right answers.
 */
public class TestQuestion {
    /**
     * @param questionId ID of question
     * @param questionText this is the text of question.
     * @param answer array of answers the question.
     * @param correctAnswerId is right answer for the question.
     */
    int questionId;
    public String questionText;
    String[] answer;
    public String correctAnswerId;

    public TestQuestion(int questionId, String questionText, String[] answer, String correctAnswerId) {

        this.questionId = questionId;
        this.questionText = questionText;
        this.answer = answer;
        this.correctAnswerId = correctAnswerId;
    }

    /**
     * There is fills array of global question arraylist.
     */
    public static void testQuestionFill() {

        TestSystem.Database.question.add(new TestQuestion(0, "Среднее арифметическое чисел у; 2,1; 3 и 2,1 равно 2,3. Найдите у: ", TestAnswer.answerMath0, TestAnswer.answerMath0[0]));

        TestSystem.Database.question.add(new TestQuestion(1, "Какое из следующих чисел делится с остатком на 36?: ", TestAnswer.answerMath1, TestAnswer.answerMath1[0]));

        TestSystem.Database.question.add(new TestQuestion(2, "Найдите сумму всех целых решений неравенства: (x - 4) / (2x + 6) ≤ 0: ", TestAnswer.answerMath2, TestAnswer.answerMath2[0]));

        TestSystem.Database.question.add(new TestQuestion(3, "Обратите периодическую дробь 0,2(6) в обыкновенную: ", TestAnswer.answerMath3, TestAnswer.answerMath3[0]));

        TestSystem.Database.question.add(new TestQuestion(4, "Найдите общий вид первообразной для функции 2sinЗх: ", TestAnswer.answerMath4, TestAnswer.answerMath4[0]));

        TestSystem.Database.question.add(new TestQuestion(5, "Упростите: (sin4a - sin6a) : (cos5a*sin a): ", TestAnswer.answerMath5, TestAnswer.answerMath5[0]));

        TestSystem.Database.question.add(new TestQuestion(6, "Число 8 составляет 30% числа b. Сколько процентов числа b + 8 составляет число b?: ", TestAnswer.answerMath6, TestAnswer.answerMath6[0]));

        TestSystem.Database.question.add(new TestQuestion(7, "Сколько сторон у выпуклого многоугольника, каждый из внутренних углов которого равен 135°?: ", TestAnswer.answerMath7, TestAnswer.answerMath7[0]));

        TestSystem.Database.question.add(new TestQuestion(8, "Стороны прямоугольника равны 72 м и 32 м. Определите сторону равновеликого ему квадрата: ", TestAnswer.answerMath8, TestAnswer.answerMath8[0]));

        TestSystem.Database.question.add(new TestQuestion(9, "Сколько всего дробей со знаменателем 36, которые больше 2/3 и меньше 5/6?: ", TestAnswer.answerMath9, TestAnswer.answerMath9[0]));

        TestSystem.Database.question.add(new TestQuestion(10, "___ - самая распространенная молекула в клетках живых организмов: ", TestAnswer.answerBio0, TestAnswer.answerBio0[0]));

        TestSystem.Database.question.add(new TestQuestion(11, "___ - идеальный растворитель в живой клетке: ", TestAnswer.answerBio1, TestAnswer.answerBio1[0]));

        TestSystem.Database.question.add(new TestQuestion(12, "«Рабочей лошадкой» генных инженеров стала: ", TestAnswer.answerBio2, TestAnswer.answerBio2[0]));

        TestSystem.Database.question.add(new TestQuestion(13, "Автотрофные организмы - это те, которые: ", TestAnswer.answerBio3, TestAnswer.answerBio3[0]));

        TestSystem.Database.question.add(new TestQuestion(14, "АТФ - это: ", TestAnswer.answerBio4, TestAnswer.answerBio4[0]));

        TestSystem.Database.question.add(new TestQuestion(15, "Болезнь несвертывания крови называется: ", TestAnswer.answerBio5, TestAnswer.answerBio5[0]));

        TestSystem.Database.question.add(new TestQuestion(16, "В состав гормонов щитовидной железы входит: ", TestAnswer.answerBio6, TestAnswer.answerBio6[0]));

        TestSystem.Database.question.add(new TestQuestion(17, "Плацента - это: ", TestAnswer.answerBio7, TestAnswer.answerBio7[0]));

        TestSystem.Database.question.add(new TestQuestion(18, "Популяция - структурная единица: ", TestAnswer.answerBio8, TestAnswer.answerBio8[0]));

        TestSystem.Database.question.add(new TestQuestion(19, "При помощи корневой системы растения поглощают из почвы: ", TestAnswer.answerBio9, TestAnswer.answerBio9[0]));

        TestSystem.Database.question.add(new TestQuestion(20, "Действие электрического тока на магнитную стрелку впервые было обнаружено ", TestAnswer.answerPhysic0, TestAnswer.answerPhysic0[0]));

        TestSystem.Database.question.add(new TestQuestion(21, "Диапазон длин волн 390 нм - 10нм занимает ", TestAnswer.answerPhysic1, TestAnswer.answerPhysic1[0]));

        TestSystem.Database.question.add(new TestQuestion(22, "Единицей магнитного потока является ", TestAnswer.answerPhysic2, TestAnswer.answerPhysic2[0]));

        TestSystem.Database.question.add(new TestQuestion(23, "Если источник света приблизить к преграде, то размер тени ", TestAnswer.answerPhysic3, TestAnswer.answerPhysic3[0]));

        TestSystem.Database.question.add(new TestQuestion(24, "Порядковый номер химического элемента в Периодической системе элементов Д.И.:" +
                " Менделеева называется ___ числом ", TestAnswer.answerPhysic4, TestAnswer.answerPhysic4[0]));

        TestSystem.Database.question.add(new TestQuestion(25, "Число колебаний в единицу времен называется___ колебаний: ", TestAnswer.answerPhysic5, TestAnswer.answerPhysic5[0]));

        TestSystem.Database.question.add(new TestQuestion(26, "Максимальная кинетическая энергия фотоэлектрона зависит от: ", TestAnswer.answerPhysic6, TestAnswer.answerPhysic6[0]));

        TestSystem.Database.question.add(new TestQuestion(27, "Между протонами в ядре действуют силы ", TestAnswer.answerPhysic7, TestAnswer.answerPhysic7[0]));

        TestSystem.Database.question.add(new TestQuestion(28, "Поток магнитной индукции Ф, пронизывающий проволочную рамку, зависит от: ", TestAnswer.answerPhysic8, TestAnswer.answerPhysic8[0]));

        TestSystem.Database.question.add(new TestQuestion(29, "Преобразование энергии электростатического " +
                "поля в электромагнитную энергию происходит в устройстве:", TestAnswer.answerPhysic9, TestAnswer.answerPhysic9[0]));

    }
}

