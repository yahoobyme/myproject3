package com.jp.trts;

/*
  @author Timirev
 * @version 1.0
 */


import com.jp.trts.model.test.Test;
import com.jp.trts.model.test.TestQuestion;
import com.jp.trts.model.user.Admin;
import com.jp.trts.model.user.Student;
import com.jp.trts.model.user.Teacher;
import com.jp.trts.model.user.User;
import com.jp.trts.view.ConsoleUI;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


/**
 * This class contains the entry point to the program, a class for creating an array of users,
 * a method for creating users,a method for finding users in arrays and
 * switch to start console users interface.
 */
public class TestSystem {
    /**
     * In this class are arrays groups of users and tests.
     */
    public static class Database {
        public static ArrayList<TestQuestion> question = new ArrayList<>();
        public static ArrayList<User> user = new ArrayList<>();
        public static ArrayList<Test> test = new ArrayList<>();

    }

    /**
     * This method adds users and tests into database arrays.
     */
    public static void insertDatabase() {
        Database.user.add(new Teacher("teacherMath", "teacher", 1, "Mathematic"));
        Database.user.add(new Teacher("teacherPhysic", "teacher", 2, "Physic"));
        Database.user.add(new Teacher("teacherBio", "teacher", 3, "Biologic"));
        Database.user.add(new Student("Ivanov", "student", 1, 0));
        Database.user.add(new Student("Smirnov", "student", 1, 0));
        Database.user.add(new Student("Kuznetsov", "student", 1, 0));
        Database.user.add(new Student("Dovydov", "student", 2, 0));
        Database.user.add(new Student("Petrov", "student", 2, 0));
        Database.user.add(new Student("Sidorov", "student", 2, 0));
        Database.user.add(new Admin("admin1", "admin"));
        Database.user.add(new Admin("admin2", "admin"));
        Database.user.add(new Admin("admin3", "admin"));
        TestQuestion.testQuestionFill();

    }

    /**
     * Fill the test array list Test objects class.
     */
    public static void insertTestData() {
        Database.test.add(new Test("Физика", 0, TestQuestionSub.questionPhysic));
        Database.test.add(new Test("Биология", 0, TestQuestionSub.questionBio));
        Database.test.add(new Test("Математика", 0, TestQuestionSub.questionMath));

    }

    /**
     * Creates sub lists of questions for thematic you need from question arraylist.
     */
    public static class TestQuestionSub {


        public static List<TestQuestion> questionBio = TestSystem.Database.question.subList(10, 20);
        public static List<TestQuestion> questionMath = TestSystem.Database.question.subList(0, 10);
        public static List<TestQuestion> questionPhysic = TestSystem.Database.question.subList(20, 30);

    }

    /**
     * Here start point of the program.
     */
    public static void main(String[] args) {

        /**
         * Adds users and tests into in memory database now.
         */
        insertDatabase();
        insertTestData();
        System.out.println("Введите имя пользователя: ");
        /**
         * User enter user name for login.
         */
        Scanner sc = new Scanner(System.in);
        String currentName = sc.nextLine();
        String searchResult = (String) searchUserByName(currentName);

        /**
         * Start console interface witch current user group.
         */

        switch (searchResult) {
            case ("teacher") -> ConsoleUI.viewTeacherUI();
            case ("student") -> ConsoleUI.viewStudentUI();
            case ("admin") -> ConsoleUI.viewAdminUI();
            default -> System.out.println("Меню недоступно! ");
        }
    }

    /**
     * This method searches for user by the entered username in Database arrays and return user group ID
     *
     * @param currentName entered user name from keyboard
     * @return userId return value user group, witch belong current username or false if user is not exist.
     */
    public static Object searchUserByName(String currentName) {

        for (int i = 0; i < Database.user.size(); i++) {
            if (currentName.contains(Database.user.get(i).userName)) {
                return Database.user.get(i).userId;
            }
        }
        return "false";

    }

}













